from jnpr.junos import Device
from pprint import pprint

print("get facts using username and ssh key ...")
with Device(host='localhost', user='root', port=2222) as dev:   
    pprint (dev.facts)
