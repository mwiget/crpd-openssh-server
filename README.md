# crpd-openssh-server

Run Juniper cRPD with ssh access for CLI and NETCONF provided by another container.
This is possible via sharing the PID namespaces, granting access to the root filesystem
of the crpd container from the ssh container.

Clone this repo and change into it:

```
$ git clone https://gitlab.com/mwiget/crpd-openssh-server.git
$ cd crpd-openssh-server
```

For this example, the sshd daemon is installed in an Alpine based container. To build 
that container, use

```
$ docker build -t alpine-ssh alpine-ssh/
```

The sshd container is pretty small:

```
$ docker images alpine-ssh                                                                
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE                                       
alpine-ssh          latest              02b2a96fa13a        6 minutes ago       13.6MB    
```

Make sure you have cRPD installed:

```
$ docker images crpd
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
crpd                20.2R1.10           41d6ae0a8fcb        6 days ago          234MB
```

Create or copy the file authorized_keys with your ssh public key into folder ./ssh/

```
$ cat ssh/authorized_keys                                                                 
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAbppm0i41M7zMFba3EWdtuxCaomRQzEdonIzXSiETsZ mwiget@may2020   
```

Now launch cRPD first, using docker, mounting its config directory (not really relevant for this test):

```
$ docker run --name r1 -it -d --rm -v $PWD/config:/config crpd:20.2R1.10
```

Check cRPD is running:

```
$ docker ps |grep r1
a125cdec9589        crpd:20.2R1.10                     "/sbin/runit-init.sh"    7 seconds ago       Up 6 seconds        22/tcp, 179/tcp, 830/tcp, 3784/tcp, 4784/tcp, 6784/tcp, 7784/tcp, 50051/tcp   r1
```

Now launch the ssh container, sharing r1's PID namespace and mount ./ssh into /root/.ssh within the container,
so your ssh public key can be used to authenticate as user root:

```
$ docker run -it -d --rm --name r1ssh \
  --pid container:r1 \
  -p 830:830 \
  -p 2222:22 \
  -v $PWD/ssh:/root/.ssh \
  alpine-ssh
```

Netconf port 830 is exposed to port 830 on the host and ssh port 22 is exposed as port 2222 on the host.

Check both containers are running:

```
$ docker ps | grep r1
e08f85d539ef        alpine-ssh                         "/bin/entrypoint.sh …"   6 seconds ago       Up 5 seconds        0.0.0.0:830->830/tcp, 0.0.0.0:2222->22/tcp                                    r1ssh
a125cdec9589        crpd:20.2R1.10                     "/sbin/runit-init.sh"    7 seconds ago       Up 6 seconds        22/tcp, 179/tcp, 830/tcp, 3784/tcp, 4784/tcp, 6784/tcp, 7784/tcp, 50051/tcp   r1
```

Finally, test ssh access

```
$ ssh -p 2222 root@localhost
The authenticity of host '[localhost]:2222 ([127.0.0.1]:2222)' can't be established.
RSA key fingerprint is SHA256:RKmm5TjR3H10WQfRzw49C+xlx5M1uQs3zvILSPjyeHY.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '[localhost]:2222' (RSA) to the list of known hosts.
groups: cannot find name for group ID 11
To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.


===>
           Containerized Routing Protocols Daemon (CRPD)
 Copyright (C) 2020, Juniper Networks, Inc. All rights reserved.
                                                                    <===

root@0aec43c35ba3:~# cli
root@7f9d8f31a9a4> show version 
Hostname: 7f9d8f31a9a4
Model: cRPD
Junos: 20.2R1.10
cRPD package version : 20.2R1.10 built by builder on 2020-06-25 12:49:34 UTC

root@7f9d8f31a9a4> show interfaces routing 
Interface        State Addresses
tunl0            Down  ISO   enabled
sit0             Down  ISO   enabled
lo.0             Up    ISO   enabled
ip6tnl0          Down  ISO   enabled
gretap0          Down  ISO   enabled
gre0             Down  ISO   enabled
eth0             Up    ISO   enabled
                       INET  172.17.0.4
erspan0          Down  ISO   enabled

root@7f9d8f31a9a4> quit 

root@0aec43c35ba3:~# exit
logout
Connection to localhost closed.
```

Now verify netconf using PyEz:

```
$ python3 ./show.py 
get facts using username and ssh key ...
{'2RE': None,
 'HOME': None,
 'RE0': None,
 'RE1': None,
 'RE_hw_mi': None,
 'current_re': None,
 'domain': None,
 'fqdn': '0aec43c35ba3',
 'hostname': '0aec43c35ba3',
 'hostname_info': {'re0': '0aec43c35ba3'},
 'ifd_style': 'CLASSIC',
 'junos_info': {'re0': {'object': junos.version_info(major=(20, 2), type=R, minor=1, build=10),
                        'text': '20.2R1.10'}},
 'master': None,
 'model': 'CRPD',
 'model_info': {'re0': 'CRPD'},
 'personality': None,
 're_info': None,
 're_master': None,
 'serialnumber': None,
 'srx_cluster': None,
 'srx_cluster_id': None,
 'srx_cluster_redundancy_group': None,
 'switch_style': 'NONE',
 'vc_capable': False,
 'vc_fabric': None,
 'vc_master': None,
 'vc_mode': None,
 'version': '20.2R1.10',
 'version_RE0': '20.2R1.10',
 'version_RE1': None,
 'version_info': junos.version_info(major=(20, 2), type=R, minor=1, build=10),
 'virtual': None}
```

All set!

A simplified method to build and launch can be found in ./start.sh.

# How it works

These cool features are used to get this working:

- Sharing the PID namespace between containers allows access to the main containers root filesystem via /proc/1/root. See the blog post under references for a description.
- sshd_config does a chroot() to the cRPD's filesystem, allowing it normal access to the cRPD's utilities, in particular the CLI
- Netconf access is granted by defining a netconf subsystem in sshd_config launching netconf (executed in the chroot of the cRPD container, despite being launched in the r1ssh container.


# References

- https://www.guidodiepen.nl/2017/04/accessing-container-contents-from-another-container/ 
- cRPD download: https://support.juniper.net/support/downloads/?p=crpd (support contract required)
- cRPD free trial: https://www.juniper.net/us/en/dm/crpd-trial/
