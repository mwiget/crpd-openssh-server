#!/bin/bash
docker build -t alpine-ssh alpine-ssh/

ssh-keygen -f ~/.ssh/known_hosts -R "[localhost]:2222" || true

echo "launching cRPD ..."
docker run --name r1 -it -d --rm -v $PWD/config:/config crpd:20.2R1.10

echo "launching openssh-server, sharing pid namespace with r1 ..."
docker run -it -d --rm --name r1ssh \
  --pid container:r1 \
  -p 830:830 \
  -p 2222:22 \
  -v $PWD/ssh:/root/.ssh \
  alpine-ssh
