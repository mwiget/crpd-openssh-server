#!/bin/sh

if [ ! -f "/etc/ssh/ssh_host_rsa_key" ]; then
    # generate fresh rsa key
      ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
fi
if [ ! -f "/etc/ssh/ssh_host_dsa_key" ]; then
    # generate fresh dsa key
      ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa
fi

chown -R root:root /root/.ssh
chmod 700 /root/.ssh
chmod 600 /root/.ssh/*

# get rid of the login messsage 'mesg: ttyname failed: No such device'
sed -i 's/^mesg/#mesg/' /proc/1/root/root/.profile
exec "$@"
